import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//import Vino.java;

public class Vinos{
    private static int menu(){
        Scanner input = new Scanner(System.in);
        int op = 0;
        String mensaje = "Menu:\n1. Nuevo Vino\n2. Nuevo Suministrador\n3. Ver info vino\n0. Salir";
        System.out.println(mensaje);
        op = input.nextInt();
        //input.close();
        return op;
    }
    //Main
    public static void main(String[] args) {
        double[] arrayEnteros = {5.5,4.12,6.32,42.1,25.3,21.1,14.3,25.3,45.3,11.3};
        List<Vino> listaVinos = new ArrayList<Vino>();
        Suministrador sum = new Suministrador("Vinos Expo","EC",20);
        
        
        //System.out.println("Hola Mundo");
        Vino.manTiposDeVino();
        Vino vino1 = new Vino("Vino Bueno", "Cavernet", 0, 125, arrayEnteros, sum);
        System.out.println("Nombre: " + vino1.getNombre());
        System.out.println("Cosecha: " + vino1.getCosecha());
        System.out.println("Tipo: " + vino1.getTipo(true));
        System.out.println("Disponibilidad: " + vino1.getDisponibilidad());
        System.out.println("Promedio 1-5: " + vino1.promediarVentas(0, 4));
        System.out.println("Promedio 6-10: " + vino1.promediarVentas(5, 9));
        System.out.println("Realizar pedido?: " + vino1.decidirPedido());
        Suministrador pro = vino1.getSuministrador();
        System.out.println("\nSuministrador");
        System.out.println("Nombre: " + pro.getNombre());
        System.out.println("Pais: " + pro.getPais());
        System.out.println("Tiempo de entrega: " + pro.getTiempoEntrega());
    }
}