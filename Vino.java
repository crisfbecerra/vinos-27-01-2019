/**
 * vino
 * La clase vino genera un nuevo vino para la lista de vinos
 */
public class Vino {
    //Parametros
    private String nombre;
    private String cosecha;
    private int tipo;// Tipo: 0 - Tinto, 1 - Blanco, 2 - Rosado
    private static String[] stringTipos = {"Tinto","Blanco","Rosado"};
    private int disponibilidad;// Cantidad de botellas disponibles
    private double[] valorVenta;
    public Suministrador suministrador;

    //-----------------------------------------------------------------//
    //Metodos
    Vino(String nombre, String cosecha, int tipo, int disponibilidad,double[] valorVenta, Suministrador sum){
        setNombre(nombre);
        setCosecha(cosecha);
        setTipo(tipo);
        setDisponibilidad(disponibilidad);
        setValoreVenta(valorVenta);
        setSuministrador(sum);
    }
    public void setNombre(String nombre) {
        if (nombre != "") {
            this.nombre = nombre;
        }
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setCosecha(String cosecha) {
        if (cosecha != "") {
            this.cosecha = cosecha;
        }
    }

    public String getCosecha() {
        return this.cosecha;
    }

    public void setTipo(int tipo) {
        if(tipo >= 0 && tipo <= 2){
            this.tipo = tipo;
        }else{
            throw new IllegalArgumentException("Valor fuera de parametros");
        }
    }
    public void setTipo(String tipo) {
        switch (tipo) {
            case "Tinto":
            case "tinto":
            case "TINTO":
                this.tipo = 0;
                break;
            case "Blanco":
            case "blanco":
            case "BLANCO":
                this.tipo = 1;
                break;
            case "Rosado":
            case "rosado":
            case "ROSADO":
                this.tipo = 2;
                break; 
            default:
                throw new IllegalArgumentException("Valor fuera de parametros");    
        }
        
    }

    public int getTipo() {
        return this.tipo;
    }

    public String getTipo(boolean op) {
        //String[] stringTipos = {"Tinto","Blanco","Rosado"};
        return Vino.stringTipos[this.tipo];
    }
    public static void manTiposDeVino(){
        System.out.println("Tipos de vino: Tinto: 0; Blanco: 1; Rosado: 2;");
    }
    public void setDisponibilidad(int disponibilidad){
        if(disponibilidad >= 0 ){
            this.disponibilidad = disponibilidad;
        }else{
            throw new IllegalArgumentException("Valor fuera de parametros");
        }
    }
    public int getDisponibilidad(){
        return this.disponibilidad; 
    }

    public void setValoreVenta(double[] valoresVenta){
        this.valorVenta = new double[valoresVenta.length];
        for(int i=0;i<valoresVenta.length;i++){
            this.valorVenta[i] = valoresVenta[i];
        }
    }

    public double[] getValoresVenta(){
        return this.valorVenta;
    }

    public void setSuministrador(Suministrador sum){
        //this.suministrador = new Suministrador(sum);
        this.suministrador = sum;
    }
    public void setSuministrador(String nombre, String pais, int dias){
        this.suministrador = new Suministrador(nombre,pais,dias);
    }
    public void setSuministrador(String nombre, String pais){
        this.suministrador = new Suministrador(nombre,pais);
    }
    public void setSuministrador(String nombre){
        this.suministrador = new Suministrador(nombre);
    }
    public Suministrador getSuministrador(){
        return this.suministrador;
    }
    public double promediarVentas(int start, int finish){
        double promedio = 0;
        int i;
        if(start > finish){
            int temp = start;
            start = finish;
            finish = temp;
        }
        for(i = start; i < finish; i++){
            promedio += this.valorVenta[i];
        }
        promedio /= i;
        return promedio;
    }

    public boolean decidirPedido(){
        if((this.suministrador.getTiempoEntrega()<30)&(promediarVentas(0, 4) < promediarVentas(5, 9))){
            return true;
        }else{
            return false;
        }
    }
    
}