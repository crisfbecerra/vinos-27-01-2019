package gui_vinos;

/**
 * Suministrador
 * Objeto Suministrador que contiene el nombre, tiempo de entrega y ubicacion
 * Ademas de sus respectivos set y get
 */
public class Suministrador {
    private String nombre;
    private String pais;
    private int tiempoEntrega;//Tiempo de entrega en dias

    //---------------------------------------------------------//

    Suministrador(String nombre, String pais, int dias){
        //Constructor con todos los parametros
        setNombre(nombre);
        setPais(pais);
        setTiempoEntrega(dias);
    }
    Suministrador(Suministrador sum){
        //Constructor copia
        this(sum.getNombre(),sum.getPais(),sum.getTiempoEntrega());
    }
    Suministrador(String nombre, String pais){
        this(nombre,pais,1);
    }
    Suministrador(String nombre){
        this(nombre,"non",1);
    }

    public void setNombre(String nombre) {
        if(nombre != ""){
            this.nombre = nombre;
        }else{
            throw new IllegalArgumentException("Nombre no puede ser en blanco");
        }
    }
    public String getNombre() {
        return this.nombre;
    }
    public void setPais(String pais){
        if(pais != ""){
            this.pais = pais;
        }else{
            throw new IllegalArgumentException("Pais no puede ser en blanco");
        }
    }
    public String getPais(){
        return this.pais;
    }
    public void setTiempoEntrega(int dias){
        if(dias > 0){
            this.tiempoEntrega = dias;
        }else{
            throw new IllegalArgumentException("Pais no puede ser en blanco");
        }
    }
    public int getTiempoEntrega(){
        return this.tiempoEntrega;
    }
    
}